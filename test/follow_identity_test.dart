/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:test/test.dart';

import 'elrepo_lib_test.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;

/// How to run tests:
/// 1. adb forward tcp:9091 tcp:9092
/// 2. Modify [identityId] variable
///
/// If you follow an identity using `rsIdentity/getIdentitiesInfo` and then
/// restart the node, when you get id details the parameter `mIsAContact` is false
///
/// pub run test test/follow_identity_test.dart -N identityFollow
/// pub run test test/follow_identity_test.dart -N identityIsContact

var identityId = '4b01ba1ab1ac85ef10cef012ea8b937e';

/// For a given [identityId] check if `mIsAContact` is set to true
Future<bool> _idIsContact (String id) async {
  return (await rs.RsIdentity.getIdentitiesInfo([identityId]))[0]['mIsAContact'];
}

void main() {
  group('Test follow identity', () {

    /// Follow an identity using `rsIdentity/getIdentitiesInfo` and check
    /// `mIsAContact`
    test('identityFollow', () async {
      AUTH.initRemote();
      assert(await rs.RsIdentity.setAsRegularContact(identityId, true));
      print("setAsRegularContact: passed");
      assert(await _idIsContact(identityId));
      print("Id is contact: passed");
    });

    /// Check if  `mIsAContact` of identity details is true
    test('identityIsContact', () async {
      AUTH.initRemote();
      assert(await _idIsContact(identityId));
      print("Id is contact: passed");
    });
  });
}