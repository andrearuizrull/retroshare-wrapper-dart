/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

abstract class ApiException implements Exception{
  late String message, reqUrl;
}

/// Exception thrown when the API error is not handled yet
///
/// If the API error is not handled, throw this exception. Check
/// [statusCodeErrorMessages] to see handled exceptions.
class ApiUnhandledErrorException extends ApiException {
  @override
  late String message;
  @override
  late String reqUrl;
  ApiUnhandledErrorException(int statusCode, this.reqUrl) {
    message = 'Unhandled statusCode: ' + statusCode.toString() + ' ' + reqUrl;
  }
  @override
  String toString() => message;
}

class LoginException extends ApiException implements Exception {
  @override
  late String message;
  @override
  late String reqUrl;
  LoginException(this.reqUrl) {
    message = 'Please, log in first ... $reqUrl';
  }
  @override
  String toString() => message;
}

class AuthFailedException extends ApiException implements Exception {
  @override
  late String message;
  @override
  late String reqUrl;
  AuthFailedException(this.reqUrl) {
    message = 'Authentication failed $reqUrl';
  }
  @override
  String toString() => message;
}

/// Return diferent Strings to throw depending the status code
String statusCodeErrorMessages(int statusCode, String path, String reqUrl) {
  switch (statusCode) {
    case 401:
      throw AuthFailedException(reqUrl);
    case 404:
      // todo(aruru): create a method not found exception
      return 'Method not found: ' + path;
    default:
      throw ApiUnhandledErrorException(statusCode, reqUrl);
  }
}
