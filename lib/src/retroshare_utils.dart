/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2022  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2022 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of retroshare;

/// Top level functions used for retrocompatibility

/// Used to wait until file hash event
///
/// Using the [RsEvents.registerEventsHandler] we wait until a
/// [RsEventType.FILE_HASHING_COMPLETED] for the defined [filePath] is fired.
///
/// It return the file hash.
Future<String> waitForFileHash(filePath) async =>
RsUtils().waitForFileHash(filePath);

/// Used to know if a directory is in the list of getSharedDirectories
Future<bool> isDirectoryAlreadyShared(String filePath) async =>
  RsUtils().isDirectoryAlreadyShared(filePath);

/// Search recursivelly on the RS directory tree until find a specified path or
/// file returning it information
Future<DirDetails?> findAFileOnDiretoryTree(String filePath,[String handle = "0"]) async =>
  RsUtils().findAFileOnDiretoryTree(filePath, handle = "0");

Future<bool> waitUntilOnline(String sslId, [int attempts = 10]) async =>
  RsUtils().waitUntilOnline(sslId, attempts);

Future<bool> waitUntilSent(String msgId, [int attempts = 5]) async =>
  RsUtils().waitUntilSent(msgId, attempts);

Future<void> addFriends(List peers) async =>
  RsUtils().addFriends(peers);

/// Function that get peers on local network and add its as friends
Future<void> broadcastPromiscuity() async =>
  RsUtils().broadcastPromiscuity();

class RsUtils {

  /// Used to wait until file hash event
  ///
  /// Using the [RsEvents.registerEventsHandler] we wait until a
  /// [RsEventType.FILE_HASHING_COMPLETED] for the defined [filePath] is fired.
  ///
  /// It return the file hash.
  Future<String> waitForFileHash(filePath) async {
    print('Start hashing file $filePath');
    var s =Stopwatch()..start();

    var resultHash = '';
    // Use this to avoid a bug hashing the first file after RS restart that don't throw the event never
    await RsFiles.forceDirectoryCheck();
    // Repeat after 20 seconds to be sure
    await RsFiles.forceDirectoryCheck(add_safe_delay: true);
    var subscription = await rsClient.registerEventsHandler(RsEventType.FILE_HASHING_COMPLETED,
            (StreamSubscription<Event>? stream, Map<String, dynamic> event) {
          print('Found Hashing event: $event');
          if (event['mFilePath'] == filePath) {
            resultHash = event['mFileHash'];
            stream?.cancel();
            stream = null;
          }
        });

    // Used to wait until result hash is done
    do {
      print('Waiting for hash... ${s.elapsed.inSeconds}s');
      await Future.delayed(Duration(milliseconds: 500));
    } while (resultHash.isEmpty);

    // This method to wait until stream close doesnt work...
    // await Future.wait([subscription.asFuture()])
    //     .whenComplete(() {
    //   print("File hashed succesful");
    // });
    print('Resulting hash for $filePath is $resultHash in ${s.elapsed.inSeconds} seconds');
    s.stop();
    return resultHash;
  }

  /// Used to know if a directory is in the list of getSharedDirectories
  Future<bool> isDirectoryAlreadyShared(String filePath) async {
    for (var share in await RsFiles.getSharedDirectories()) {
      if (share['filename'] == filePath) {
        print('$filePath already shared');
        return true;
      }
    }
    return false;
  }

  /// Search recursivelly on the RS directory tree until find a specified path or
  /// file returning it information
  Future<DirDetails?> findAFileOnDiretoryTree(String filePath,
      [String handle = "0"]) async {
    var directory = await RsFiles.requestDirDetails(handle);
    if (directory.children.isNotEmpty) {
      // First check to see if requested filePath is in this directory
      for (var child in directory.children) {
        if (child['name'] == filePath) {
          return RsFiles.requestDirDetails(child['handle']['xstr64']);
        }
      }
      // If isn't start the recursive search
      for (var child in directory.children) {
        var childDetails =
        await findAFileOnDiretoryTree(filePath, child['handle']['xstr64']);
        if (childDetails != null) return childDetails;
      }
    }
    return null;
  }

  Future<bool> waitUntilOnline(String sslId, [int attempts = 10]) async {
    // wait a bit until the peer accepts us
    for (; attempts >= 0; attempts--) {
      try {
        var ids = await RsPeers.getOnlineList();
        if ((ids is List) && ids.contains(sslId)) return true; // done

        // else => no news yet => retry
        await RsPeers.connectAttempt(sslId).catchError((_) {});

        if (attempts == 0) {
          // do not wait on the last iteration
          return false;
        }

        // else => retry
        await Future.delayed(Duration(seconds: 5));
      } catch (err) {
        await Future.delayed(Duration(seconds: 5));
      }
    }
    return false;
  }

  Future<bool> waitUntilSent(String msgId, [int attempts = 5]) async {
    try {
      for (; attempts >= 0; attempts--) {
        final response = await RsMsgs.getMessage(msgId);
        if (response == null) {
          return false;
        } else if (!(response is Map) ||
            (response['msgflags'] & RsMsgs.RS_MSG_PENDING) != 0) {
          // still pending
          await Future.delayed(Duration(seconds: 5));
          continue;
        }
        return true;
      }
      return false;
    } catch (err) {
      return false;
    }
  }

  Future<void> addFriends(List peers) async {
    Map peer;
    for (peer in peers) {
      print('Adding Friend: $peer');
      final sslId = peer['mSslId'];
      final alreadyFriend = await RsPeers.isFriend(sslId);
      if (!alreadyFriend) {
        final pgpFingerprint = peer['mPgpFingerprint'];
        final pgpId = pgpFingerprint.substring(pgpFingerprint.length - 16);
        final uri = peer['mLocator']['urlString'];
        final parsedUri = Uri.parse(uri);
        final details = {
          'localAddr': parsedUri.host,
          'localPort': parsedUri.port
        };
        RsPeers.addSslOnlyFriend(sslId, pgpId, details);
      }
    }
  }

  /// Function that get peers on local network and add its as friends
  Future<void> broadcastPromiscuity() async {
    print('Runing Broadcarst Promisquity');
    final peers = await RsBroadcastDiscovery.getDiscoveredPeers();
    if (peers.isNotEmpty) {
      print('Peers found: $peers');
      addFriends(peers);
    }
  }
}
