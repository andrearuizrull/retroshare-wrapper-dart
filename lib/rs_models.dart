/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

library rs_models;

import 'dart:convert';
import 'dart:typed_data';
part 'src/rs_models/response_type_models/rs_gxs_forums.dart';
part 'src/rs_models/response_type_models/rs_events.dart';
part 'src/rs_models/response_type_models/rs_gxs_circles.dart';
part 'src/rs_models/response_type_models/rs_gxs_image.dart';
part 'src/rs_models/common.dart';
part 'src/rs_models/chat_type_models/chat_lobby_id.dart';
part 'src/rs_models/chat_type_models/distance_chat_peer_nfo.dart';
part 'src/rs_models/response_type_models/rs_files.dart';
part 'src/rs_models/chat_type_models/chat_id_type.dart';
part 'src/rs_models/chat_type_models/chat_id.dart';
part 'src/rs_models/response_type_models/rs_time.dart';
part 'src/rs_models/chat_type_models/visible_chat_lobby_record.dart';
part 'src/rs_models/chat_type_models/chat.dart';
part 'src/rs_models/chat_type_models/chat_message.dart';
part 'src/rs_models/request_type_models/req_create_chat_lobby.dart';
part 'src/rs_models/response_type_models/rs_location.dart';
part 'src/rs_models/response_type_models/rs_identity.dart';
